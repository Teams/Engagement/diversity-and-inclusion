The Diversity and Inclusion Initiative seeks to promote diversity and
inclusion within the GNOME Project through a series of actions that drive changes within the project and the GNOME community. These actions will help drive the following objectives:

* Better Outreach to under-represented minority communities
* Retain community members through improved onboarding, and communications
* Better opportunities to attend events, conferences, and hackfests

It's been shown that when a project has good diversity it means that we have built a community around our project that sufficiently welcoming and productive. The totality of the human experience across genders, culture, race, and location can be a powerful force is achieving our ends. Such a community would be a powerful force in building a desktop that represents the people using it.

How to be part of GNOME Diversity and Inclusion.

* Matrix: https://matrix.to/#/!gDYcfFnIBrAySOLnNn:gnome.org?via=gnome.org
* Discourse: https://discourse.gnome.org/

Feel free to post on discourse and chat on matrix about anything and everything on GNOME Diversity and Inclusion efforts.

Further reading:
* https://www.linuxfoundation.org/research/the-2021-linux-foundation-report-on-diversity-equity-and-inclusion-in-open-source
* https://thenewstack.io/how-chaoss-di-can-help-diversity-in-the-open-source-community/
* https://github.com/chaoss/wg-dei/tree/main/demographic-data
